# kIP Front-end
## Reference Caddy2 configuration file
```
kip.digilol.net {
        encode zstd gzip
        reverse_proxy /api/* unix//run/varnishd.sock
        reverse_proxy /graphql/* 172.16.2.2:8000
        redir / /ip/?ip={http.request.remote.host}
        redir /graphql /graphql/
        root * /srv/digilol/www/kip.digilol.net
        file_server
}
```
