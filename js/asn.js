function kipGetParams() {
	queryString = window.location.search;
	urlParams = new URLSearchParams(queryString);
	asn = urlParams.get("asn")
	kipASNquery(asn)
}

function kipSearch() {
	asn = document.getElementById("asn_input").value
	kipASNquery(asn)
}

function kipASNquery(asn) {
	fetch("https://kip.digilol.net/api/asn/" + asn)
	.then((response) => {
		if (response.ok) {
			return response.json()
		} else {
			throw new Error("NETWORK RESPONSE ERROR")
		}
	})
	.then(data => {
		console.log(data)
		displayASNinfo(asn, data)
	})
	.catch((error) => console.error("FETCH ERROR:", error))
}

function displayASNinfo(asn, data) {
	table = document.getElementById("asn_table")
	table.innerHTML = ""

	tbody = document.createElement("tbody")
	tbody.appendChild(infoEntry("ASN", asn))
	tbody.appendChild(infoEntry("Organization", data.org))
	if (data["mnt-by"] != null) {tbody.appendChild(infoEntry("mnt-by", data["mnt-by"].join(", ")))}
	if (data.notify != null) {tbody.appendChild(infoEntry("notify", data.notify.join(", ")))}
	tbody.appendChild(infoEntry("prefixes", data.prefixes.join(", ")))
	table.appendChild(tbody)
}

function infoEntry(key, value) {
	tr = document.createElement("tr")
	th = document.createElement("th")
	td = document.createElement("td")

  	th.innerHTML = key
	tr.appendChild(th)

	td.innerHTML = value
	tr.appendChild(td)
	return tr
}

kipGetParams()
