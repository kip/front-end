function kipGetParams() {
        queryString = window.location.search;
        urlParams = new URLSearchParams(queryString);
        ip = urlParams.get("ip")
        kipIPquery(ip)
}

function kipSearch() {
	ip = document.getElementById("ip_input").value
	kipIPquery(ip)
}

function kipIPquery(ip) {
	fetch("https://kip.digilol.net/api/ip/" + ip)
	.then((response) => {
		if (response.ok) {
			return response.json()
		} else {
			throw new Error("NETWORK RESPONSE ERROR")
		}
	})
	.then(data => {
		console.log(data)
		displayIPinfo(ip, data)
	})
	.catch((error) => console.error("FETCH ERROR:", error))
}

function displayIPinfo(ip, data) {
	table = document.getElementById("ip_table")
	table.innerHTML = ""

	tbody = document.createElement("tbody")
	tbody.appendChild(infoEntry("IP", ip))
	tbody.appendChild(infoEntry("Hostname", data.hostname))
	tbody.appendChild(infoEntry("ASN", "<a href=/as/?asn="+ data.asn +">"+ data.asn + "</a>"))
	tbody.appendChild(infoEntry("Country", data.country))
	tbody.appendChild(infoEntry("Organization", data.org))
	tbody.appendChild(infoEntry("Route", data.route))
	tbody.appendChild(infoEntry("Tor", data.services.tor))
	table.appendChild(tbody)
}

function infoEntry(key, value) {
	tr = document.createElement("tr")
	th = document.createElement("th")
	td = document.createElement("td")

  	th.innerHTML = key
	tr.appendChild(th)

	td.innerHTML = value
	tr.appendChild(td)
	return tr
}

kipGetParams()
